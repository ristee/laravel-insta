<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = [];

    public function profileImage()
    {
        $notavailableImage = '/image/not-available.jpg';
        $image = ($this->image ? ('/storage/'.$this->image) : $notavailableImage);

        return $image;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class);
    }
}
