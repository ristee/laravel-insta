init: down build up

up:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

tinker:
	docker-compose run --rm app php artisan tinker

command:
	docker-compose run --rm app php artisan $(command)

migrations:
	docker-compose run --rm app php artisan migrate

composer-require:
	docker-compose run --rm app composer require $(package)
