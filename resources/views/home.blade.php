@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-3 p-5">
            <img
                class="img-thumbnail rounded-circle"
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Logo_TV_2015.svg/1200px-Logo_TV_2015.svg.png"
                alt="">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <h1>{{ $user->username }}</h1>
                <a href="#">Add new Post</a>
            </div>
            <div class="d-flex">
                <div class="pr-5"><strong>123</strong> posts</div>
                <div class="pr-5"><strong>23k</strong> followeres</div>
                <div class="pr-5"><strong>11</strong> following</div>
            </div>
            <div class="pt-4 font-weight-bold">{{ $user->profile->title }}</div>
            <div>{{ $user->profile->description }}</div>
            <div><a href="{{ $user->profile->url }}">{{ $user->profile->url }}</a></div>
        </div>
    </div>
    <div class="row pt-4">
        <div class="col-4">
            <img class="w-100"
                src="https://upload.wikimedia.org/wikipedia/commons/d/de/Windows_live_square.JPG"
                alt="">
        </div>
        <div class="col-4">
            <img class="w-100"
                src="https://upload.wikimedia.org/wikipedia/commons/d/de/Windows_live_square.JPG"
                alt="">
        </div><div class="col-4">
            <img class="w-100"
                src="https://upload.wikimedia.org/wikipedia/commons/d/de/Windows_live_square.JPG"
                alt="">
        </div>
    </div>
</div>
@endsection
